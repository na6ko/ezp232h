DEVICE = {
	0x000000 => {:name => "error all 0", :capacity => 0},
	0xffffff => {:name => "error all 1", :capacity => 0},
	0x620613 => {:name => "SST25PF040C", 
		:freq => 40_000_000, :capacity => 0x80000, 
		#typ 40 ms, max 150 ms
		:erase_time_sector => {:first => 30_000, :every => 10_000}, 
		#typ 4 ms, max 5 ms
		:program_time => {:first => 2_000, :every => 500}
	}
}

#---- common ----
def flash_init
	freq_set 6_000_000
	spi_cs_set 1
	spi_cs_set 0
	spi_send [0xab, 0, 0, 0] #release from deep power down
	spi_cs_set 1
	spi_cs_set 0
	d = [0x9f, 0xff, 0xff, 0xff] #JEDEC ID read
	spi_xfer d
	spi_cs_set 1
	id = 0
	d[1,3].each{|t|
		id <<= 8
		id |= t
	}
	if DEVICE.key?(id) == false
		printf("unknown device id: %06x\n", id)
		return {:capacity => 0}
	end
	r = DEVICE[id]
	puts "device: " + r[:name]
	r
end
def address_set(opcode, address)
	command = [opcode]
	[16, 8, 0].each{|shift|
		command << ((address >> shift) & 0xff)
	}
	command
end

#---- read ----
def read_main(address, length)
	d = Array.new(length, 0xec)
	command = address_set(0x0b, address) #read memory at higher speed
	command << 0xff
	spi_cs_set 0
	spi_send command
	spi_xfer d
	spi_cs_set 1
	d
end

#---- program and erase ----
def program_wait(t)
	waittime = t[:first]
	read_status = [0x05, 0b11]
	while (read_status[1] & 1) == 1
		read_status = [0x05, 0b11] #read status register
		usleep waittime
		spi_cs_set 0
		spi_xfer read_status
		spi_cs_set 1
		waittime = t[:every]
	end
end

def write_enable
	spi_cs_set 0
	spi_send [0x06] #write enable
	spi_cs_set 1
end

PROGRAM_PAGE_SIZE = 0x100
PROGRAM_PAGE_ALLFF = Array.new(PROGRAM_PAGE_SIZE, 0xff)
PROGRAM_ADDRESS_MASK = 0xffff00
PROGRAM_ALIGN_MASK = PROGRAM_ADDRESS_MASK ^ 0xffffff
ERASE_SECTOR_SIZE = 0x10000
ERASE_ADDRESS_MASK = 0xff0000
ERASE_ALIGN_MASK = ERASE_ADDRESS_MASK ^ 0xffffff

def program_sequence(d, force_erase, address, data)
	if force_erase || ((address & ERASE_ALIGN_MASK) == 0)
		write_enable
		spi_cs_set 0
		spi_send(address_set(0xd8, address & ERASE_ADDRESS_MASK)) #erase 64 Kbyte block of memory array
		spi_cs_set 1
		program_wait d[:erase_time_sector]
	end
	if PROGRAM_PAGE_ALLFF == data
		return
	end
	
	wdata = address_set(0x02, address & PROGRAM_ADDRESS_MASK) #to program up to 256 bytes
	wdata.concat data
	write_enable
	spi_cs_set 0
	spi_send wdata
	spi_cs_set 1
	program_wait d[:program_time]
end
def program_main(d, flash_address, data)
	length = data.size
	#1page に収まるdata
	if (flash_address & PROGRAM_ADDRESS_MASK) == ((flash_address + length) & PROGRAM_ADDRESS_MASK)
		wdata = Array.new(PROGRAM_PAGE_SIZE, 0xff)
		wdata[flash_address & PROGRAM_ALIGN_MASK, length] = data
		program_sequence(d, true, flash_address, wdata)
		length = 0
	end

	force_erase = true
	programmed = 0
	#先頭の alignment 補正
	alignment = flash_address & PROGRAM_ALIGN_MASK
	if length != 0 && alignment != 0
		printf("%3d%%", programmed * 100 / data.size)
		l = PROGRAM_PAGE_SIZE - alignment
		wdata = Array.new(PROGRAM_PAGE_SIZE, 0xff)
		wdata[alignment, l] = data[0, l]
		program_sequence(d, force_erase, flash_address, wdata)
		force_erase = false
		flash_address &= PROGRAM_ADDRESS_MASK
		flash_address += PROGRAM_PAGE_SIZE
		length -= l
		programmed += l
	end

	while length >= PROGRAM_PAGE_SIZE
		printf("\r%3d%%", programmed * 100 / data.size)
		program_sequence(d, force_erase, flash_address, data[programmed, PROGRAM_PAGE_SIZE])
		force_erase = false
		flash_address += PROGRAM_PAGE_SIZE
		length -= PROGRAM_PAGE_SIZE
		programmed += PROGRAM_PAGE_SIZE
	end
	
	#末尾の alignment 補正
	if length != 0
		printf("\r%3d%%", programmed * 100 / data.size)
		wdata = Array.new(PROGRAM_PAGE_SIZE, 0xff)
		wdata[0, length] = data[programmed, length]
		program_sequence(d, force_erase, flash_address, wdata)
	end
	puts("\rprogramming done")
end

#---- user method ---
USAGE = <<EOS
flash_program [filename] [flash_address]
--- description ----
program fileimage into flash memory
--- arguments---
filename: input filename
flash_address: flash memory destination address; eg: 0x10000
--- note ---
flash memory sector size is aligned 0x10000 bytes.
If flash_address 0x000800 is , address 0x00000-0x0007ff is erased.
flash memory page size aligned 0x100 bytes. unaligned data is stored 0xff.

flash_read [filename] (flash_address=0) (flash_size)
flash_verify [filename] (flash_address=0) (flash_size)
flash_dump [flash_dump] [flash_size]
--- description ----
flash_read: read and store flash memory data into file
flash_verify: read and compare flash memory data from file
flash_dump: read and dump flash memory data into stdout
--- arguments---
filename: (flash_read) output filename, (flash_verify) input filename
flash_address: source address, if it is not specified, address is 0
flash_size: read size, if it is not specified, size is capacity - address
EOS
def usage
	puts USAGE
end

def flash_program(filename, flash_address)
	d = flash_init
	if d[:capacity] == 0
		return
	end
	if d[:freq] != nil
		freq_set d[:freq]
	end
	if File.size(filename) > d[:capacity]
		puts "input filesize is over memory capacity"
		return
	end
	data = File.open(filename, 'rb').read.unpack('C*')
	flash_address = flash_address.to_i(0)
	
	if (data.size + flash_address) > d[:capacity]
		puts "data range overflow"
		return
	end
	program_main(d, flash_address, data)
	rdata = read_main(flash_address, data.size)
	if(rdata == data)
		puts("verify ok")
	else
		puts("verify error")
	end
end

def init_read(flash_address, size)
	d = flash_init
	if d[:capacity] == 0
		return nil
	end
	if d[:freq] != nil
		freq_set d[:freq]
	end
	if flash_address != 0
		flash_address = flash_address.to_i(0)
	end
	if size == nil
		size = d[:capacity]
	else
		size = size.to_i(0)
	end
	if (flash_address + size) > d[:capacity]
		size = d[:capacity] - size
	end
	read_main(flash_address, size)
end

def flash_read(filename, flash_address = 0, size = nil)
	data = init_read(flash_address, size)
	if data == nil
		return
	end
	f = File.open(filename, "wb")
	f.write(data.pack('C*'))
	f.close
end

def flash_verify(filename, flash_address = 0, size = nil)
	if size == nil
		size = File.size(filename)
	end
	rdata = init_read(flash_address, size)
	if rdata == nil
		return
	end
	if rdata.size != File.size(filename)
		puts "file size is not match"
		return
	end
	
	if rdata == File.open(filename, 'rb').read.unpack('C*')
		puts "verify ok"
	else
		puts "verify error"
	end
end

def flash_dump(flash_address, size)
	data = init_read(flash_address, size)
	if data == nil
		return
	end

	flash_address = flash_address.to_i(0)
	(flash_address & 0xfffff0).step(flash_address + size.to_i(0) - 1, 0x10){|a|
		hex = ""
		char = ""
		0x10.times{|i|
			v = nil
			if (a + i) >= flash_address
				v = data.shift
			end
			if v == nil
				hex << '  '
				char << ' '
			else
				hex << sprintf("%02X", v)
				if v < 0x20 || v >= 0x80
					char << '.'
				else
					char << v.chr
				end
			end
			case(i)
			when 3, 7, 11
				hex << '-'
			else
				hex << ' '
			end
		}
		printf("%06X:%s %s\n", a, hex, char)
	}
end

#---- debug ----
def flash_read_test
	flash_read("hoge.bin", 0x78000, 0x8000)
end
def flash_program_test
	flash_program("programmer.o", 0x78001)
end
