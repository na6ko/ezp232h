#ifndef _MPSSE_DRIVER_H_
#define _MPSSE_DRIVER_H_
struct mpsse_driver{
	int (*write)(void *t, uint8_t *data, int length);
	int (*read)(void *t, uint8_t *data, int length);
	void (*close)(void *t);
};
#endif
