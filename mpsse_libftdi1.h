#ifndef _MPSSE_LIBFTDI1_
#define _MPSSE_LIBFTDI1_
struct ftdi1;
struct mpsse_driver;
struct ftdi1 *libftdi1_init(const struct mpsse_driver **driver, int ifnum, const char *devstr);
#endif
