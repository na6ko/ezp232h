#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include "mpsse.h"
static int i2c_eeprom_address_set(struct mpsse *m, uint8_t control, int address)
{
	mpsse_i2c_start(m);
	if(mpsse_i2c_send(m, control | 0) != 0){
		return 0;
	}
	mpsse_i2c_send(m, address >> 8);
	mpsse_i2c_send(m, address);
	return 1;
}
static int i2c_eeprom_program(struct mpsse *m, uint8_t control, const uint8_t *data, int address, int length)
{
	while(length != 0){
		const int l = length < 0x40 ? length : 0x40;
		if(i2c_eeprom_address_set(m, control, address) == 0){
			return 0;
		}
		for(int i = 0; i < l; i++){
			mpsse_i2c_send(m, *data++);
		}
		mpsse_i2c_stop(m);
		uint8_t r;
		do{
			usleep(100);
			mpsse_i2c_start(m);
			r = mpsse_i2c_send(m, control | 0);
			mpsse_i2c_stop(m);
		}while(r != 0);
		
		length -= l;
	}
	return 1;
}
static int i2c_eeprom_read(struct mpsse *m, uint8_t control, int address, int length)
{
	if(i2c_eeprom_address_set(m, control, address) == 0){
		return 0;
	}
	mpsse_i2c_start(m);
	mpsse_i2c_send(m, control | 1);
	for(int i = 0; i < length; i++){
		uint8_t d;
		mpsse_i2c_receive(m, &d, i != (address - 1));
		char c = ' ';
		switch(i & 0xf){
		case 3: case 7: case 11:
			c = '-';
			break;
		case 15:
			c = '\n';
			break;
		}
		printf("%02x%c", d, c);
	}
	mpsse_i2c_stop(m);
	return 1;
}
static int spi_flash_read(struct mpsse *m, int address, int length)
{
	uint8_t d[4] = {0xab, 0, 0, 0}; //release power-down
	mpsse_spi_cs_set(m, 1);
	mpsse_spi_cs_set(m, 0);
	mpsse_spi_send(m, d, sizeof(d));
	mpsse_spi_cs_set(m, 1);
	d[0] = 0x9f; //read JEDEC ID
	mpsse_spi_cs_set(m, 0);
	mpsse_spi_xfer(m, d, sizeof(d));
	mpsse_spi_cs_set(m, 1);
	
	printf("device id:%02x %02x %02x\n", d[1], d[2], d[3]);
	static const uint8_t a0[3] = {0, 0, 0};
	static const uint8_t a1[3] = {0xff, 0xff, 0xff};
	if(
		memcmp(a0, d + 1, sizeof(a0)) == 0 ||
		memcmp(a0, d + 1, sizeof(a1)) == 0
	){
		return 0;
	}
	return 1;
}
int main(int c, const char **v)
{
	struct mpsse *m = mpsse_init(0, NULL, 1000000);
	if(m == NULL){
		return 1;
	}
	if(0){
		const int M24FC256_CONTROL = (0b1010 << 4) | (0b111 << 1);
		uint8_t temp[0x40];
		memset(temp, 0x55, sizeof(temp));
		for(int i = 0; i < sizeof(temp); i += 4){
			temp[i] += i;
		}
		int r = 1;
		r = i2c_eeprom_program(m, M24FC256_CONTROL, temp, 0x4a40, sizeof(temp));
		if(r){
			i2c_eeprom_read(m, M24FC256_CONTROL, 0x4a40, sizeof(temp));
		}
	}else{
		spi_flash_read(m, 0x00000, 0x20);
	}
	mpsse_close(m);
	return 0;
}
