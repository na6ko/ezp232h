#define _GNU_SOURCE
#include <assert.h>
#include <ftd2xx.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdio.h>
#include <string.h>
#include "mpsse_d2xx.h"
#include "mpsse_driver.h"
struct d2xx{
	FT_HANDLE h;
};

static int x_ft_write(void *tt, uint8_t *data, int length)
{
	struct d2xx *t = tt;
	DWORD rc;
	FT_STATUS s = FT_Write(t->h, data, length, &rc);
	assert(s == FT_OK);
	return rc;
}

static int x_ft_read(void *tt, uint8_t *data, int length)
{
	struct d2xx *t = tt;
	DWORD rc;
	FT_STATUS s = FT_Read(t->h, data, length, &rc);
	assert(s == FT_OK);
	return rc;
}

static void x_ft_close(void *tt)
{
	struct d2xx *t = tt;
	FT_STATUS s = FT_SetBitMode(t->h, 0, FT_BITMODE_RESET);
	assert(s == FT_OK);
	FT_Close(t->h);
}

static const struct mpsse_driver d2xx_driver = {
	x_ft_write, x_ft_read, x_ft_close
};
static bool device_open_byid(struct d2xx *t, DWORD searchid)
{
	DWORD num, flag, type, id, location;
	char serialnumber[16], descripition[64];
	bool found = false;
	int r = -1;
	FT_STATUS s = FT_CreateDeviceInfoList(&num);
	assert(s == FT_OK);
	for(DWORD i = 0; i < num; i++){
		s = FT_GetDeviceInfoDetail(i, &flag, &type, &id, &location, serialnumber, descripition, &t->h);
		assert(s == FT_OK);
		if(searchid == id){
			if(found == true){
				fprintf(stderr, "same ID devices are connected.\n");
				return false;
			}
			r = i;
			found = true;
		}
	}
	if(found == false){
		fprintf(stderr, "Can't find iCE FTDI USB device.\n");
		return false;
	}
	s = FT_Open(r, &t->h);
	assert(s == FT_OK);
	return true;
}
struct d2xx *d2xx_init(const struct mpsse_driver **driver, int ifnum, const char *devstr)
{
	*driver = &d2xx_driver;
	static struct d2xx dev;
	dev.h = NULL;
	FT_STATUS s = FT_OK;
	if(devstr != NULL){
		//-d "d:Single RS232-HS"
		if(memcmp("d:", devstr, 2) == 0){
			devstr += 2;
			char temp[0x40];
			strncpy(temp, devstr, sizeof(temp) - 1);
			s = FT_OpenEx(temp, FT_OPEN_BY_DESCRIPTION, &dev.h);
			if(s == FT_DEVICE_NOT_FOUND){
				fprintf(stderr, "Can't find iCE FTDI USB device (device string %s).\n", devstr);
				return NULL;
			}
		}else
		//-d i:0x0403:0x6014
		if(memcmp("i:", devstr, 2) == 0){
			devstr += 2;
			char *e;
			DWORD id = strtoul(devstr, &e, 0);
			if(*e != ':'){
				return NULL;
			}
			id <<= 16;
			devstr = e + 1;
			id |= strtoul(devstr, &e, 0);
			if(device_open_byid(&dev, id) == false){
				return NULL;
			}
		}else{
			return NULL;
		}
	}else if(device_open_byid(&dev, 0x04036014) == true){
		//nop;
	}else if(device_open_byid(&dev, 0x04036010) == true){
		//nop;
	}else{
		return NULL;
	}
	assert(s == FT_OK);
	//mpsse_ftdic_open = true;
	
	s = FT_ResetDevice(dev.h);
	assert(s == FT_OK);
	s = FT_SetLatencyTimer(dev.h, 2);
	assert(s == FT_OK);
	s = FT_Purge(dev.h, FT_PURGE_RX | FT_PURGE_TX);
	assert(s == FT_OK);
	s = FT_SetBitMode(dev.h, 0xff, FT_BITMODE_MPSSE);
	assert(s == FT_OK);
	
	return &dev;
}
