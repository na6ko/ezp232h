/*
 *  iceprog -- simple programming tool for FTDI-based Lattice iCE
 *programmers
 *
 *  Copyright (C) 2015  Clifford Wolf <clifford@clifford.at>
 *  Copyright (C) 2018  Piotr Esden-Tempski <piotr@esden.net>
 *
 *  Permission to use, copy, modify, and/or distribute this software
 *for any
 *  purpose with or without fee is hereby granted, provided that the
 *above
 *  copyright notice and this permission notice appear in all copies.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 *WARRANTIES
 *  WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 *  MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE
 *FOR
 *  ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY
 *DAMAGES
 *  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN
 *AN
 *  ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING
 *OUT OF
 *  OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/
#define _GNU_SOURCE
#include <libftdi1/ftdi.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include "mpsse_libftdi1.h"
#include "mpsse_driver.h"
struct ftdi1{
	struct ftdi_context mpsse_ftdic;
	bool mpsse_ftdic_open;
	bool mpsse_ftdic_latency_set;
	unsigned char mpsse_ftdi_latency;
};
static int mou_write(void *tt, uint8_t *data, int length)
{
	struct ftdi1 *t = tt;
	return ftdi_write_data(&t->mpsse_ftdic, data, length);
}

static int mou_read(void *tt, uint8_t *data, int length)
{
	struct ftdi1 *t = tt;
	return ftdi_read_data(&t->mpsse_ftdic, data, length);
}

static void mou_close(void *tt)
{
	struct ftdi1 *t = tt;
	if(t->mpsse_ftdic_open){
		if(t->mpsse_ftdic_latency_set){
			ftdi_set_latency_timer(&t->mpsse_ftdic, t->mpsse_ftdi_latency);
		}
		ftdi_disable_bitbang(&t->mpsse_ftdic);
		ftdi_usb_close(&t->mpsse_ftdic);
	}
	ftdi_deinit(&t->mpsse_ftdic);
}

static const struct mpsse_driver ftdi1_driver = {
	mou_write, mou_read, mou_close
};

struct ftdi1 *libftdi1_init(const struct mpsse_driver **driver, int ifnum, const char *devstr)
{
	static struct ftdi1 dev;
	*driver = &ftdi1_driver;
	dev.mpsse_ftdic_open = false;
	dev.mpsse_ftdic_latency_set = false;

	enum ftdi_interface ftdi_ifnum = INTERFACE_A;

	switch(ifnum){
	case 0:
		ftdi_ifnum = INTERFACE_A;
		break;
	case 1:
		ftdi_ifnum = INTERFACE_B;
		break;
	case 2:
		ftdi_ifnum = INTERFACE_C;
		break;
	case 3:
		ftdi_ifnum = INTERFACE_D;
		break;
	default:
		ftdi_ifnum = INTERFACE_A;
		break;
	}

	ftdi_init(&dev.mpsse_ftdic);
	ftdi_set_interface(&dev.mpsse_ftdic, ftdi_ifnum);

	if(devstr != NULL){
		if(ftdi_usb_open_string(&dev.mpsse_ftdic, devstr)){
			fprintf(stderr, "Can't find iCE FTDI USB device (device string %s).\n", devstr);
			mou_close(&dev);
			return NULL;
		}
	}else{
		if(ftdi_usb_open(&dev.mpsse_ftdic, 0x0403, 0x6010) && ftdi_usb_open(&dev.mpsse_ftdic, 0x0403, 0x6014)){
			fprintf(stderr, "Can't find iCE FTDI USB device (vendor_id 0x0403, device_id 0x6010 or 0x6014).\n");
			mou_close(&dev);
			return NULL;
		}
	}
	dev.mpsse_ftdic_open = true;
	if(ftdi_usb_reset(&dev.mpsse_ftdic)){
		fprintf(stderr, "Failed to reset iCE FTDI USB device.\n");
		mou_close(&dev);
		return NULL;
	}

	if(ftdi_usb_purge_buffers(&dev.mpsse_ftdic)){
		fprintf(stderr, "Failed to purge buffers on iCE FTDI USB device.\n");
		mou_close(&dev);
		return NULL;
	}

	if(ftdi_get_latency_timer(&dev.mpsse_ftdic, &dev.mpsse_ftdi_latency) < 0){
		fprintf(stderr, "Failed to get latency timer (%s).\n", ftdi_get_error_string(&dev.mpsse_ftdic));
		mou_close(&dev);
		return NULL;
	}

	/* 1 is the fastest polling, it means 1 kHz polling */
	if(ftdi_set_latency_timer(&dev.mpsse_ftdic, 1) < 0){
		fprintf(stderr, "Failed to set latency timer (%s).\n", ftdi_get_error_string(&dev.mpsse_ftdic));
		mou_close(&dev);
		return NULL;
	}

	dev.mpsse_ftdic_latency_set = true;

	/* Enter MPSSE (Multi-Protocol Synchronous Serial Engine) mode.
	   Set all pins to output. */
	if(ftdi_set_bitmode(&dev.mpsse_ftdic, 0xff, BITMODE_MPSSE) < 0){
		fprintf(stderr, "Failed to set BITMODE_MPSSE on iCE FTDI USB device.\n");
		mou_close(&dev);
		return NULL;
	}
	return &dev;
}
