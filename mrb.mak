CC = clang
CFLAGS = -Wall -Werror -Imingw64/include -I../iceprogrammer/cdm
#CFLAGS += -O2
CFLAGS += -O0 -g
OBJ = mrb.o $(addprefix mpsse, .o _d2xx.o _libftdi1.o)
mrb.exe: $(OBJ)
	$(CC) -o $@ $^ -lmruby -lws2_32 -lftdi1 ../iceprogrammer/cdm/amd64/ftd2xx.lib
clean:
	rm -f $(OBJ) mrb.exe
