/*
 *  iceprog -- simple programming tool for FTDI-based Lattice iCE
 *programmers
 *
 *  Copyright (C) 2015  Clifford Wolf <clifford@clifford.at>
 *  Copyright (C) 2018  Piotr Esden-Tempski <piotr@esden.net>
 *
 *  Permission to use, copy, modify, and/or distribute this software
 *for any
 *  purpose with or without fee is hereby granted, provided that the
 *above
 *  copyright notice and this permission notice appear in all copies.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 *WARRANTIES
 *  WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 *  MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE
 *FOR
 *  ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY
 *DAMAGES
 *  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN
 *AN
 *  ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING
 *OUT OF
 *  OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 *  Relevant Documents:
 *  -------------------
 * 
 *http://www.ftdichip.com/Support/Documents/AppNotes/AN_108_Command_Processor_for_MPSSE_and_MCU_Host_Bus_Emulation_Modes.pdf
 */

#define _GNU_SOURCE
#include <assert.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <unistd.h>
#include "mpsse.h"
#include "mpsse_driver.h"
#include "mpsse_d2xx.h"
#include "mpsse_libftdi1.h"

struct mpsse{
	void *klass;
	const struct mpsse_driver *driver;
};
//---------------------------------------------------------
//MPSSE / FTDI definitions
//---------------------------------------------------------

/* FTDI bank pinout typically used for iCE dev boards
 * BUS IO | Signal | Control
 * -------+--------+--------------
 * xDBUS0 |    SCK | MPSSE
 * xDBUS1 |   MOSI | MPSSE
 * xDBUS2 |   MISO | MPSSE
 * xDBUS3 |     nc |
 * xDBUS4 |     CS | GPIO
 * xDBUS5 |     nc |
 * xDBUS6 |  CDONE | GPIO
 * xDBUS7 | CRESET | GPIO
 */

/* MPSSE engine command definitions */
enum mpsse_cmd {
	/* Mode commands */
	MC_SETB_LOW = 0x80, /* Set Data bits LowByte */
	MC_READB_LOW = 0x81, /* Read Data bits LowByte */
	MC_SETB_HIGH = 0x82, /* Set Data bits HighByte */
	MC_READB_HIGH = 0x83, /* Read data bits HighByte */
	MC_LOOPBACK_EN = 0x84, /* Enable loopback */
	MC_LOOPBACK_DIS = 0x85, /* Disable loopback */
	MC_SET_CLK_DIV = 0x86, /* Set clock divisor */
	MC_FLUSH = 0x87, /* Flush buffer fifos to the PC. */
	MC_WAIT_H = 0x88, /* Wait on GPIOL1 to go high. */
	MC_WAIT_L = 0x89, /* Wait on GPIOL1 to go low. */
	MC_TCK_X5 = 0x8A, /* Disable /5 div, enables 60MHz master clock */
	MC_TCK_D5 = 0x8B, /* Enable /5 div, backward compat to FT2232D */
	MC_EN_3PH_CLK = 0x8C, /* Enable 3 phase clk, DDR I2C */
	MC_DIS_3PH_CLK = 0x8D, /* Disable 3 phase clk */
	MC_CLK_N = 0x8E, /* Clock every bit, used for JTAG */
	MC_CLK_N8 = 0x8F, /* Clock every byte, used for JTAG */
	MC_CLK_TO_H = 0x94, /* Clock until GPIOL1 goes high */
	MC_CLK_TO_L = 0x95, /* Clock until GPIOL1 goes low */
	MC_EN_ADPT_CLK = 0x96, /* Enable adaptive clocking */
	MC_DIS_ADPT_CLK = 0x97, /* Disable adaptive clocking */
	MC_CLK8_TO_H = 0x9C, /* Clock until GPIOL1 goes high, count bytes */
	MC_CLK8_TO_L = 0x9D, /* Clock until GPIOL1 goes low, count bytes */
	MC_TRI = 0x9E, /* Set IO to only drive on 0 and tristate on 1 */
	/* CPU mode commands */
	MC_CPU_RS = 0x90, /* CPUMode read short address */
	MC_CPU_RE = 0x91, /* CPUMode read extended address */
	MC_CPU_WS = 0x92, /* CPUMode write short address */
	MC_CPU_WE = 0x93, /* CPUMode write extended address */
};

/* Transfer Command bits */

/* All byte based commands consist of:
 * - Command byte
 * - Length lsb
 * - Length msb
 *
 * If data out is enabled the data follows after the above command
 *bytes,
 * otherwise no additional data is needed.
 * - Data * n
 *
 * All bit based commands consist of:
 * - Command byte
 * - Length
 *
 * If data out is enabled a byte containing bitst to transfer follows.
 * Otherwise no additional data is needed. Only up to 8 bits can be
 *transferred
 * per transaction when in bit mode.
 */

/* b 0000 0000
 *   |||| |||`- Data out negative enable. Update DO on negative clock
 *edge.
 *   |||| ||`-- Bit count enable. When reset count represents bytes.
 *   |||| |`--- Data in negative enable. Latch DI on negative clock
 *edge.
 *   |||| `---- LSB enable. When set clock data out LSB first.
 *   ||||
 *   |||`------ Data out enable
 *   ||`------- Data in enable
 *   |`-------- TMS mode enable
 *   `--------- Special command mode enable. See mpsse_cmd enum.
 */

#define MC_DATA_TMS  (0x40) /* When set use TMS mode */
#define MC_DATA_IN   (0x20) /* When set read data (Data IN) */
#define MC_DATA_OUT  (0x10) /* When set write data (Data OUT) */
#define MC_DATA_LSB  (0x08) /* When set input/output data LSB first. */
#define MC_DATA_ICN  (0x04) /* When set receive data on negative clock
	                       edge */
#define MC_DATA_BITS (0x02) /* When set count bits not bytes */
#define MC_DATA_OCN  (0x01) /* When set update data on negative clock
	                       edge */

//---------------------------------------------------------
//MPSSE / FTDI function implementations
//---------------------------------------------------------

static void mpsse_check_rx(struct mpsse *t)
{
	while(1){
		uint8_t data;
		if(t->driver->read(t->klass, &data, 1) <= 0){
			break;
		}
		fprintf(stderr, "unexpected rx byte: %02X\n", data);
	}
}

void mpsse_error(struct mpsse *t, int status)
{
	mpsse_check_rx(t);
	fprintf(stderr, "ABORT.\n");
	t->driver->close(t->klass);
	exit(status);
}

static void mpsse_recv_data(struct mpsse *t, uint8_t *data, int length)
{
	assert(length <= 0x10000);
	t->driver->read(t->klass, data, length);
}
#if 0
static uint8_t mpsse_recv_byte(struct mpsse *t)
{
	uint8_t data;
	mpsse_recv_data(t, &data, 1);
	return data;
}
#endif
static void mpsse_send_data(struct mpsse *t, uint8_t *data, int length)
{
	assert(length <= 0x10000);
	int rc = t->driver->write(t->klass, data, length);
	if(rc != length){
		fprintf(stderr, "Write error (%s, rc=%d, expected %d).\n", __FUNCTION__, rc, 1);
		mpsse_error(t, 2);
	}
}
#if 0
static void mpsse_send_byte(struct mpsse *t, uint8_t data)
{
	mpsse_send_data(t, &data, 1);
}
#endif
static void send_spi(struct mpsse *t, uint8_t *data, int n, uint8_t command)
{
	if(n < 1){
		return;
	}

	/* Output only, update data on negative clock edge. */
	uint8_t d[] = {
		command,
		n - 1, (n - 1) >> 8
	};
	mpsse_send_data(t, d, sizeof(d));
	mpsse_send_data(t, data, n);
}
void mpsse_spi_send(struct mpsse *t, uint8_t *data, int n)
{
	while(n > 0){
		int l = 0x10000;
		if(l > n){
			l = n;
		}
		send_spi(t, data, l, MC_DATA_OUT | MC_DATA_OCN);
		data += l;
		n -= l;
	}
}

void mpsse_spi_xfer(struct mpsse *t, uint8_t *data, int n)
{
	while(n > 0){
		int l = 0x10000;
		if(l > n){
			l = n;
		}
		send_spi(t, data, l, MC_DATA_IN | MC_DATA_OUT | MC_DATA_OCN);
		mpsse_recv_data(t, data, l);
		data += l;
		n -= l;
	}
}

#if 0
void mpsse_gpio_set(struct mpsse *t, uint8_t gpio, uint8_t direction)
{
	uint8_t d[] = {MC_SETB_LOW, gpio, direction};
	mpsse_send_data(t, d, sizeof(d));
}

int mpsse_readb_low(struct mpsse *t)
{
	mpsse_send_byte(t, MC_READB_LOW);
	return mpsse_recv_byte(t);
}

int mpsse_readb_high(struct mpsse *t)
{
	mpsse_send_byte(t, MC_READB_HIGH);
	return mpsse_recv_byte(t);
}

void mpsse_dummy_send_bytes(struct mpsse *t, uint8_t n)
{
	//add 8 x count dummy bits (aka n bytes)
	uint8_t d[] = {MC_CLK_N8, n - 1, 0x00};
	mpsse_send_data(t, d, sizeof(d));
}

void mpsse_dummy_send_bit(struct mpsse *t)
{
	//add 1  dummy bit
	uint8_t d[] = {MC_CLK_N, 0x00};
	mpsse_send_data(t, d, sizeof(d));
}
#endif
#define PORT_MODE (1 << 6)
#define PORT_MODE_SPI (1 << 6)
#define PORT_MODE_I2C (0 << 6)
#define PORT_SPI_SCK (1 << 0)
#define PORT_SPI_DI (1 << 1)
#define PORT_SPI_CS (1 << 4)
void mpsse_spi_cs_set(struct mpsse *t, uint8_t cs)
{
	uint8_t d [] = {
		MC_SETB_LOW, PORT_MODE_SPI,
		PORT_MODE | PORT_SPI_CS | PORT_SPI_SCK | PORT_SPI_DI
	};
	if(cs){
		d[1] |= PORT_SPI_CS;
	}
	mpsse_send_data(t, d, sizeof(d));
}
/*
AD SPI   I2C
0  o SCK o SCL
1  o DI  b SDA(input)
2  i DO  i SDA(output)
3  x x   x x
4  o CS  o A0
5- o L   o H   (mode)

I2C A2:0 = 3'b111
*/
#define PORT_I2C_SCL (1 << 0)
#define PORT_I2C_DO (1 << 1)
#define PORT_I2C_DI (1 << 2)
#define PORT_I2C_A0 (1 << 4)
#define I2C_OUT_SIZE (3)
#define I2C_IN_SIZE (3)
static inline void i2c_out(uint8_t *d, int scl, int sda)
{
	*d++ = MC_SETB_LOW;
	uint8_t out = PORT_I2C_A0 | PORT_MODE_I2C;
	uint8_t dir = PORT_I2C_A0 | PORT_MODE | PORT_I2C_SCL;
	if(scl){
		out |= PORT_I2C_SCL;
	}
	if(sda == 0){ //sda == 1, dir is input, sda is pulluped High
		dir |= PORT_I2C_DO;
	}
	*d++ = out;
	*d++ = dir;
}
void mpsse_i2c_start(struct mpsse *t)
{
	uint8_t data[I2C_OUT_SIZE * 3], *d = data;
	i2c_out(d, 1, 1);
	d += I2C_OUT_SIZE;
	i2c_out(d, 1, 0);
	d += I2C_OUT_SIZE;
	i2c_out(d, 0, 0);
	mpsse_send_data(t, data, sizeof(data));
}
void mpsse_i2c_stop(struct mpsse *t)
{
	uint8_t data[I2C_OUT_SIZE * 2], *d = data;
	i2c_out(d, 1, 0);
	d += I2C_OUT_SIZE;
	i2c_out(d, 1, 1);
	mpsse_send_data(t, data, sizeof(data));
}
static inline void i2c_in(uint8_t *d)
{
	*d++ = MC_SETB_LOW;
	*d++ = PORT_I2C_A0 | PORT_MODE_I2C; //OUT
	*d++ = PORT_I2C_A0 | PORT_MODE | PORT_I2C_SCL; //DIR
}
uint8_t mpsse_i2c_send(struct mpsse *t, const uint8_t data)
{
	uint8_t command[4 + I2C_IN_SIZE + 2 + 1 + I2C_OUT_SIZE];
	uint8_t r = 0;
	{
		uint8_t *c = command;
		//send 8bits via mpsse
		*c++ = MC_DATA_OUT | MC_DATA_OCN;
		*c++ = 0; *c++ = 0; //size = 1 - 1
		*c++ = data;
		//get ack
		i2c_in(c);
		c += I2C_IN_SIZE;
		*c++ = MC_DATA_IN | MC_DATA_BITS;
		*c++ = 0;
		*c++ = MC_FLUSH;
		//after reading ack, dir is out
		i2c_out(c, 0, 0);
		c += I2C_OUT_SIZE;
		assert((c-command) == sizeof(command));
		mpsse_send_data(t, command, sizeof(command));
	}
	mpsse_recv_data(t, &r, 1);
	return r;
}
void mpsse_i2c_receive(struct mpsse *t, uint8_t *data, int ack)
{
	uint8_t command[I2C_IN_SIZE + 3 + 1];
	{
		uint8_t *c = command;
		i2c_in(c);
		c += I2C_IN_SIZE;
		//receive 8 bits via mpsse
		*c++ = MC_DATA_IN;
		*c++ = 0; *c++ = 0; //size = 1 - 1
		//
		*c++ = MC_FLUSH;
		mpsse_send_data(t, command, sizeof(command));
		mpsse_recv_data(t, data, 1);
		if(ack){
			c = command;
			//send ack
			i2c_out(c, 0, 0);
			c += I2C_OUT_SIZE;
			i2c_out(c, 1, 0);
			c += I2C_OUT_SIZE;
			mpsse_send_data(t, command, c - command);
		}
	}
}
struct mpsse *mpsse_init(int ifnum, const char *devstr)
{
	static struct mpsse m;
	m.klass = d2xx_init(&m.driver, ifnum, devstr);
	if(m.klass == NULL){
		m.klass = libftdi1_init(&m.driver, ifnum, devstr);
	}
	if(m.klass == NULL){
		return NULL;
	}
	return &m;
}
int mpsse_freq_set(struct mpsse *t, int clock_hz)
{
	uint8_t data[4], *d = data;
	int baseclock = 6000000 * 10;
	if(clock_hz < (baseclock / 0x10000)){
		baseclock /= 5;
		*d++ = MC_TCK_D5;
		if(clock_hz > (baseclock / 0x10000)){
			return 0;
		}
	}else{
		*d++ = MC_TCK_X5;
	}
	//60M / 6M -> 10
	//60M / 40M -> 1
	int div = baseclock / clock_hz - 1;
	if(baseclock % clock_hz){
		div += 1;
	}
	*d++ = MC_SET_CLK_DIV;
	*d++= div;
	div >>= 8;
	*d++= div;
	mpsse_send_data(t, data, d - data);
	return 1;
}

void mpsse_close(struct mpsse *t)
{
	t->driver->close(t->klass);
}
