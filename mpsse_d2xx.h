#ifndef _MPSSE_D2XX_
#define _MPSSE_D2XX_
struct d2xx;
struct mpsse_driver;
struct d2xx *d2xx_init(const struct mpsse_driver **driver, int ifnum, const char *devstr);
#endif
