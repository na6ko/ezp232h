.PHONY: all clean
FTD2XX_PATH = ../iceprogrammer/cdm
CC = clang
CFLAGS = -Wall -Werror -I$(FTD2XX_PATH)
#CFLAGS += -O2
CFLAGS += -g -O0
OBJ = $(addprefix mpsse, .o _d2xx.o _libftdi1.o) programmer.o
LIB = $(FTD2XX_PATH)/amd64/ftd2xx.lib -lftdi1
TARGET = programmer.exe
all: $(TARGET)

$(TARGET): $(OBJ)
	$(CC) -o $@ $^ $(LIB)
clean:
	rm -f $(OBJ) $(TARGET)
