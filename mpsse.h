/*
 *  iceprog -- simple programming tool for FTDI-based Lattice iCE programmers
 *
 *  Copyright (C) 2015  Clifford Wolf <clifford@clifford.at>
 *  Copyright (C) 2018  Piotr Esden-Tempski <piotr@esden.net>
 *
 *  Permission to use, copy, modify, and/or distribute this software for any
 *  purpose with or without fee is hereby granted, provided that the above
 *  copyright notice and this permission notice appear in all copies.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 *  WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 *  MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 *  ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 *  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 *  ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 *  OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef MPSSE_H
#define MPSSE_H

#include <stdint.h>

struct mpsse;
void mpsse_error(struct mpsse *t, int status);
void mpsse_spi_send(struct mpsse *t, uint8_t *data, int n);
void mpsse_spi_xfer(struct mpsse *t, uint8_t *data, int n);
uint8_t mpsse_spi_xfer_bits(struct mpsse *t, uint8_t data, int n);
void mpsse_spi_cs_set(struct mpsse *t, uint8_t cs);
void mpsse_i2c_start(struct mpsse *t);
void mpsse_i2c_stop(struct mpsse *t);
uint8_t mpsse_i2c_send(struct mpsse *t, const uint8_t data);
void mpsse_i2c_receive(struct mpsse *t, uint8_t *data, int send_ack);
#if 0
void mpsse_gpio_set(struct mpsse *t, uint8_t gpio, uint8_t direction);
int mpsse_readb_low(struct mpsse *t);
int mpsse_readb_high(struct mpsse *t);
void mpsse_dummy_send_bytes(struct mpsse *t, uint8_t n);
void mpsse_dummy_send_bit(struct mpsse *t);
#endif
struct mpsse *mpsse_init(int ifnum, const char *devstr);
int mpsse_freq_set(struct mpsse *t, int clock_hz);
void mpsse_close(struct mpsse *t);

#endif /* MPSSE_H */
