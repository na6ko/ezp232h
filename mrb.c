#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <mruby.h>
#include <mruby/array.h>
#include <mruby/string.h>
#include <mruby/compile.h>
#include <mruby/error.h>
#include "mpsse.h"
static struct mpsse *mpsse;
/*
size = 0 は許容するが data = NULL とする.
range error は size は保持, data = NULL.
size = 0 またはすべての data が range を満たす場合は return 1.
*/
struct array{
	mrb_value mrb_array;
	mrb_int size;
	uint8_t *data;
};

static int array_into_uint8_t(mrb_state *m, struct array *t)
{
	t->size = ARY_LEN(RARRAY(t->mrb_array));
	if(t->size == 0){
		t->data = NULL;
		return 1;
	}
	t->data = malloc(t->size);
	for(mrb_int i = 0; i < t->size; i++){
		mrb_int v = mrb_fixnum(mrb_ary_ref(m, t->mrb_array, i));
		if(v < 0 || v >= 0x100){
			free(t->data);
			t->data = NULL;
			return 0;
		}
		t->data[i] = v;
	}
	return 1;
}
static mrb_value m_usleep(mrb_state *m, mrb_value self)
{
	mrb_value d;
	mrb_get_args(m, "i", &d);
	usleep(mrb_fixnum(d));
	return mrb_nil_value();
}
static mrb_value m_freq_set(mrb_state *m, mrb_value self)
{
	assert(mpsse != NULL);
	mrb_value d;
	mrb_get_args(m, "i", &d);
	mrb_int v = mrb_fixnum(d);
	if(v < 91){
		mrb_sys_fail(m, "freq is too low");
		return mrb_nil_value();
	}
	return mpsse_freq_set(mpsse, v) == 0 ? mrb_false_value() : mrb_true_value();
}
static mrb_value m_i2c_start(mrb_state *m, mrb_value self)
{
	assert(mpsse != NULL);
	mpsse_i2c_start(mpsse);
	return mrb_nil_value();
}
static mrb_value m_i2c_stop(mrb_state *m, mrb_value self)
{
	assert(mpsse != NULL);
	mpsse_i2c_stop(mpsse);
	return mrb_nil_value();
}
static mrb_value m_i2c_send(mrb_state *m, mrb_value self)
{
	assert(mpsse != NULL);
	mrb_value d;
	mrb_get_args(m, "i", &d);
	mrb_int v = mrb_fixnum(d);
	if(v < 0 || v >= 0x100){
		mrb_sys_fail(m, "uint8_t range error");
		return mrb_nil_value();
	}
	mpsse_i2c_send(mpsse, v);
	return mrb_nil_value();
}
static mrb_value m_i2c_receive(mrb_state *m, mrb_value self)
{
	assert(mpsse != NULL);
	mrb_value ack;
	mrb_get_args(m, "i", &ack);
	uint8_t d;
	mpsse_i2c_receive(mpsse, &d, mrb_fixnum(ack));
	return mrb_fixnum_value(d);
}
static mrb_value m_spi_cs_set(mrb_state *m, mrb_value self)
{
	assert(mpsse != NULL);
	mrb_value cs;
	mrb_get_args(m, "i", &cs);
	mpsse_spi_cs_set(mpsse, mrb_fixnum(cs));
	return mrb_nil_value();
}
static mrb_value spi_send_main(mrb_state *m, void (*callback)(mrb_state *m, struct array *t))
{
	assert(mpsse != NULL);
	struct array t;
	mrb_get_args(m, "A", &t.mrb_array);
	if(array_into_uint8_t(m, &t) == 0){
		mrb_sys_fail(m, "uint8_t range error");
		return mrb_nil_value();
	}
	if(t.size == 0){
		mrb_sys_fail(m, "array size is 0");
		return mrb_nil_value();
	}
	(*callback)(m, &t);
	free(t.data);
	return mrb_nil_value();
}
static void spi_send_callback(mrb_state *m, struct array *t)
{
	mpsse_spi_send(mpsse, t->data, t->size);
}
static mrb_value m_spi_send(mrb_state *m, mrb_value self)
{
	return spi_send_main(m, spi_send_callback);
}
static void spi_xfer_callback(mrb_state *m, struct array *t)
{
	mpsse_spi_xfer(mpsse, t->data, t->size);
	for(int i = 0; i < t->size; i++){
		mrb_ary_set(m, t->mrb_array, i, mrb_fixnum_value(t->data[i]));
	}
}
static mrb_value m_spi_xfer(mrb_state *m, mrb_value self)
{
	return spi_send_main(m, spi_xfer_callback);
}
static const struct funclist{
	const char *name;
	mrb_value (*func)(mrb_state *m, mrb_value self);
	int argc;
}list[] = {
#define M(NAME, ARGC) {#NAME, m_##NAME, ARGC}
	M(usleep, 1), //i
	M(freq_set, 1), //i
	M(i2c_start, 0),
	M(i2c_stop, 0),
	M(i2c_send, 1), //i
	M(i2c_receive, 1), //i
	M(spi_cs_set, 1), //i
	M(spi_send, 1), //A
	M(spi_xfer, 1), //A
	{NULL, NULL, 0}
#undef M
};

static int script_exec(const char *filename, const char *funcname, int argc, const char **c_argv)
{
	if(argc > 10){
		puts("argc is too many, argc > 10");
		return -1;
	}
	FILE *const f = fopen(filename, "r");
	if(f == NULL){
		puts("file is not found");
		return 1;
	}
	mrb_state *m = mrb_open();
	{
		const struct funclist *l = list;
		while(l->name != NULL){
			mrb_define_method(m, m->kernel_module, l->name, l->func, MRB_ARGS_REQ(l->argc));
			l++;
		}
	}
	int arena = mrb_gc_arena_save(m);
	mrb_value rbfile;
	{
		mrbc_context *c = mrbc_context_new(m);
		mrbc_filename(m, c, filename);
		rbfile = mrb_load_file_cxt(m, f, c);
		mrbc_context_free(m, c);
	}
	fclose(f);
	if(m->exc != 0 && (mrb_nil_p(rbfile) || mrb_undef_p(rbfile))){
		mrb_print_error(m);
		//m->exc = 0;
		mrb_close(m);
		return -2;
	}
	mrb_gc_arena_restore(m, arena);
	
	mrb_value m_argv[10];
	for(int i = 0; i < argc; i++){
		m_argv[i] = mrb_str_new_cstr(m, c_argv[i]);
	}
	mrb_funcall_argv(m, rbfile, mrb_intern_cstr(m, funcname), argc, m_argv);
	if(m->exc != 0){
		mrb_print_error(m);
		//m->exc = 0;
		mrb_close(m);
		return -2;
	}
	mrb_close(m);
	return 0;
}
int main(int c, const char **v)
{
	mpsse = NULL;
	if(c < 3){
		puts("[scriptfile] [functionname]");
		return -1;
	}
	mpsse = mpsse_init(0, NULL);
	if(mpsse == NULL){
		puts("mpsse device is not found");
		return -1;
	}
	const char *filename = v[1];
	const char *funcname = v[2];
	v += 3;
	c -= 3;
	int r = script_exec(filename, funcname, c ,v);
	mpsse_close(mpsse);
	return r;
}
